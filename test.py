from cottonwood.loggers import ConfusionLogger
from cottonwood.structure import load_structure
from data import TestingData

model_filename = "classifier.pkl"

debug = False
if debug:
    n_testing_iterations = 1e2
else:
    # There are 10,000 test images in all
    n_testing_iterations = 1e4

# Retrieve model
model = load_structure(model_filename)
model.add(TestingData(), "data")
model.connect("data", "input_norm", i_port_tail=0)
model.connect("data", "onehot", i_port_tail=1)

# Test the model on the full set of test examples 
total_loss = 0
confusion_logger = ConfusionLogger(n_iter_report=n_testing_iterations)
for i_iter in range(int(n_testing_iterations)):
    model.forward_pass()
    total_loss += model.blocks["loss"].loss
    confusion_logger.log_values(
        model.blocks["hardmax"].result,
        model.blocks["onehot"].result,
        model.blocks["onehot"].get_labels())

print(f"loss: {total_loss / n_testing_iterations}")
print(f"accuracy: {confusion_logger.calculate_accuracy()}")
